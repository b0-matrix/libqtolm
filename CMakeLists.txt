cmake_minimum_required(VERSION 3.1)

set(API_VERSION "3.0")
project(qtolm VERSION "${API_VERSION}.1" LANGUAGES CXX)

include(CheckCXXCompilerFlag)
if (WIN32)
    if (NOT CMAKE_INSTALL_LIBDIR)
        set(CMAKE_INSTALL_LIBDIR ".")
    endif ()

    if (NOT CMAKE_INSTALL_BINDIR)
        set(CMAKE_INSTALL_BINDIR ".")
    endif ()

    if (NOT CMAKE_INSTALL_INCLUDEDIR)
        set(CMAKE_INSTALL_INCLUDEDIR "include")
    endif ()
else ()
    include(GNUInstallDirs)
    set(INCLUDEDIR_INIT ${PROJECT_NAME})
endif (WIN32)
set(${PROJECT_NAME}_INSTALL_INCLUDEDIR
        "${CMAKE_INSTALL_INCLUDEDIR}/${INCLUDEDIR_INIT}" CACHE PATH
        "directory to install ${PROJECT_NAME} include files to")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# Set a default build type if none was specified
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to 'Debug' as none was specified")
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build" FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "MinSizeRel" "RelWithDebInfo")
endif()

foreach (FLAG all "" pedantic extra error=return-type no-unused-parameter no-gnu-zero-variadic-macro-arguments)
    CHECK_CXX_COMPILER_FLAG("-W${FLAG}" WARN_${FLAG}_SUPPORTED)
    if ( WARN_${FLAG}_SUPPORTED AND NOT CMAKE_CXX_FLAGS MATCHES "(^| )-W?${FLAG}($| )")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -W${FLAG}")
    endif ()
endforeach ()

find_package(Qt5 5.9 REQUIRED Core)

# olm
find_package(Olm 3.0.0 REQUIRED)

message( STATUS )
message( STATUS "=============================================================================" )
message( STATUS "                       libqtolm Build Information" )
message( STATUS "=============================================================================" )
message( STATUS "Version: ${PROJECT_VERSION}, API version: ${API_VERSION}")
if (CMAKE_BUILD_TYPE)
    message( STATUS "Build type: ${CMAKE_BUILD_TYPE}")
endif(CMAKE_BUILD_TYPE)
message( STATUS "Using compiler: ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}" )
message( STATUS "Using Qt ${Qt5_VERSION}" )
message( STATUS "=============================================================================" )
message( STATUS )

# Set up source files
set(libqtolm_SRCS
    lib/account.cpp
    lib/account.h
    lib/groupsession.cpp
    lib/groupsession.h
    lib/message.cpp
    lib/message.h
    lib/pk.cpp
    lib/pk.h
    lib/session.cpp
    lib/session.h
    lib/utils.h
    lib/errors.h
)

add_library(QtOlm ${libqtolm_SRCS})
set_property(TARGET QtOlm PROPERTY VERSION "${PROJECT_VERSION}")
set_property(TARGET QtOlm PROPERTY SOVERSION ${API_VERSION} )
set_property(TARGET QtOlm PROPERTY
             INTERFACE_QtOlm_MAJOR_VERSION ${API_VERSION})
set_property(TARGET QtOlm APPEND PROPERTY
             COMPATIBLE_INTERFACE_STRING QtOlm_MAJOR_VERSION)

target_compile_features(QtOlm PUBLIC cxx_std_14)

target_include_directories(QtOlm PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/lib>
    $<INSTALL_INTERFACE:${${PROJECT_NAME}_INSTALL_INCLUDEDIR}>
)
target_link_libraries(QtOlm Olm::Olm Qt5::Core)

configure_file(QtOlm.pc.in ${CMAKE_CURRENT_BINARY_DIR}/QtOlm.pc @ONLY NEWLINE_STYLE UNIX)

add_subdirectory(tests)

# Installation

install(TARGETS QtOlm EXPORT QtOlmTargets
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        INCLUDES DESTINATION ${${PROJECT_NAME}_INSTALL_INCLUDEDIR}
)
install(DIRECTORY lib/ DESTINATION ${${PROJECT_NAME}_INSTALL_INCLUDEDIR}
        FILES_MATCHING PATTERN "*.h")

include(CMakePackageConfigHelpers)
# NB: SameMajorVersion doesn't really work yet, as we're within 0.x trail.
# Maybe consider jumping the gun and releasing 1.0, as semver advises?
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/QtOlm/QtOlmConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
)

export(PACKAGE QtOlm)
export(EXPORT QtOlmTargets
    FILE "${CMAKE_CURRENT_BINARY_DIR}/QtOlm/QtOlmTargets.cmake")
configure_file(cmake/QtOlmConfig.cmake
    "${CMAKE_CURRENT_BINARY_DIR}/QtOlm/QtOlmConfig.cmake"
    COPYONLY
)

set(ConfigFilesLocation "${CMAKE_INSTALL_LIBDIR}/cmake/QtOlm")
install(EXPORT QtOlmTargets
        FILE QtOlmTargets.cmake DESTINATION ${ConfigFilesLocation})

install(FILES cmake/QtOlmConfig.cmake
    "${CMAKE_CURRENT_BINARY_DIR}/QtOlm/QtOlmConfigVersion.cmake"
    DESTINATION ${ConfigFilesLocation}
)
# Only available from CMake 3.7; reserved for future use
#install(EXPORT_ANDROID_MK QtOlmTargets DESTINATION share/ndk-modules)

if (WIN32)
    install(FILES mime/packages/freedesktop.org.xml DESTINATION mime/packages)
endif (WIN32)

if (UNIX AND NOT APPLE)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/QtOlm.pc DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)
endif()
