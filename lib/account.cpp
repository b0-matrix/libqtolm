#include "account.h"

#include "errors.h"
#include "session.h"
#include "utils.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QPair>
#include <QtDebug>

using namespace QtOlm;

Account::Account(QObject* parent) : QObject(parent), _account(newAccount()) {
  size_t randomSize = olm_create_account_random_length(_account);
  QByteArray randomData = getRandomData(randomSize);
  checkErr(olm_create_account(_account, randomData.data(), randomSize));
}

Account::Account(QByteArray pickle, QString passphrase, QObject* parent)
    : QObject(parent), _account(newAccount()) {
  if (pickle.isEmpty())
    throw new InvalidArgument("Pickle is empty");
  std::string pass = passphrase.toStdString();
  checkErr(olm_unpickle_account(_account, pass.data(), pass.length(),
                                pickle.data(), pickle.length()));
}

Account::~Account() {
  olm_clear_account(_account);
}

QByteArray Account::pickle(QString passphrase) {
  std::string pass = passphrase.toStdString();
  size_t pickleLength = olm_pickle_account_length(_account);
  QByteArray pickleBuffer(pickleLength, '0');
  checkErr(olm_pickle_account(_account, pass.data(), pass.length(),
                              pickleBuffer.data(), pickleLength));
  return pickleBuffer;
}

QPair<QByteArray, QByteArray> Account::identityKeys() {
  size_t keyLength = olm_account_identity_keys_length(_account);
  QByteArray keyBuffer(keyLength, '0');
  checkErr(olm_account_identity_keys(_account, keyBuffer.data(), keyLength));
  QJsonObject key = QJsonDocument::fromJson(keyBuffer).object();
  return QPair<QByteArray, QByteArray>(
      key.value("curve25519").toString().toUtf8(),
      key.value("ed25519").toString().toUtf8());
}

QByteArray Account::curve25519IdentityKey() {
  return identityKeys().first;
}

QByteArray Account::ed25519IdentityKey() {
  return identityKeys().second;
}

QByteArray Account::sign(QString message) {
  std::string msg = message.toStdString();
  size_t sigLength = olm_account_signature_length(_account);
  QByteArray sigBuffer(sigLength, '0');
  checkErr(olm_account_sign(_account, msg.data(), msg.length(),
                            sigBuffer.data(), sigLength));
  return sigBuffer;
}

QByteArray Account::sign(QJsonObject message) {
  return sign(QJsonDocument(message).toJson(QJsonDocument::Compact));
}

int Account::maxOneTimeKeys() {
  return int(olm_account_max_number_of_one_time_keys(_account));
}

void Account::markKeysAsPublished() {
  olm_account_mark_keys_as_published(_account);
}

void Account::generateOneTimeKeys(int count) {
  size_t randomLength =
      olm_account_generate_one_time_keys_random_length(_account, count);
  QByteArray randomBuffer = getRandomData(randomLength);
  checkErr(olm_account_generate_one_time_keys(
      _account, count, randomBuffer.data(), randomLength));
}

QJsonObject Account::oneTimeKeys() {
  size_t keyLength = olm_account_one_time_keys_length(_account);
  QByteArray keyBuffer(keyLength, '0');
  checkErr(olm_account_one_time_keys(_account, keyBuffer.data(), keyLength));
  return QJsonDocument::fromJson(keyBuffer).object();
}

QVariantHash Account::curve25519OneTimeKeys() {
  return oneTimeKeys().value("curve25519").toObject().toVariantHash();
}

QVariantHash Account::ed25519OneTimeKeys() {
  return oneTimeKeys().value("ed25519").toObject().toVariantHash();
}

void Account::removeOneTimeKeys(Session* session) {
  checkErr(olm_remove_one_time_keys(_account, session->session()));
}

OlmAccount* Account::newAccount() {
  return olm_account(new uint8_t[olm_account_size()]);
}

void Account::checkErr(size_t code) {
  if (code != olm_error())
    return;
  std::string lastError = olm_account_last_error(_account);

  if (lastError == "SUCCESS")
    return;
  if (lastError == "NOT_ENOUGH_RANDOM")
    throw new EntropyError(lastError);
  if (lastError == "OUTPUT_BUFFER_TOO_SMALL" ||
      lastError == "OLM_INPUT_BUFFER_TOO_SMALL")
    throw new BufferError(lastError);
  if (lastError == "BAD_MESSAGE_VERSION" || lastError == "BAD_MESSAGE_FORMAT" ||
      lastError == "BAD_MESSAGE_MAC" || lastError == "BAD_MESSAGE_KEY_ID" ||
      lastError == "UNKNOWN_MESSAGE_INDEX")
    throw new MessageError(lastError);
  if (lastError == "INVALID_BASE64")
    throw new Base64Error(lastError);
  if (lastError == "BAD_ACCOUNT_KEY")
    throw new AccountKeyError(lastError);
  if (lastError == "UNKNOWN_PICKLE_VERSION" ||
      lastError == "CORRUPTED_PICKLE" ||
      lastError == "BAD_LEGACY_ACCOUNT_PICKLE")
    throw new PickleError(lastError);
  if (lastError == "BAD_SESSION_KEY")
    throw new SessionKeyError(lastError);
  if (lastError == "BAD_SIGNATURE")
    throw new SignatureError(lastError);
  throw new OlmError(lastError);
}
